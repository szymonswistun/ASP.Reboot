﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ASP.Reboot.Models;
using Microsoft.AspNet.Identity;

namespace ASP.Reboot.Controllers {
    public class LinksController : Controller {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Links
        [Route("links/private/list")]
        public ActionResult Index() {
            if (User.Identity.IsAuthenticated) {
                string id = User.Identity.GetUserId();
                var dbList = (from l in db.Links
                              where l.UserID == id
                              select l).AsEnumerable<Link>();

                return View(dbList);
            }
            else {
                return RedirectToRoute("Default");
            }
        }

        [Route("links/public/list")]
        public ActionResult PublicList() {
            string uid = User.Identity.GetUserId();
            var list = ((User.IsInRole("Admin")) ?
                (from l in db.Links
                 select l) :
                (from l in db.Links
                 where l.publiczny == true || l.UserID == uid
                 select l)).AsEnumerable<Link>();
            ViewData["Admin"] = User.IsInRole("Admin");

            return View(list);
        }

        // GET: Links/Details/5
        public ActionResult Details(int? id) {
            if (id == null) {
                return RedirectToRoute("Default");
            }
            Link link = db.Links.Find(id);
            if (link == null) {
                return HttpNotFound();
            }
            var uId = link.UserID;
            ViewData["author"] = (from u in db.Users
                                  where u.Id == uId
                                  select u).AsEnumerable().ElementAt(0).UserName.TrimEnd(new char[] { ';', '"' }).TrimStart('"');
            return View(link);
        }

        // GET: Links/Create
        public ActionResult Create() {
            return View();
        }

        // POST: Links/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "nazwa,url,publiczny")] Link linkObj) {
            if (ModelState.Values.ElementAt(0).Errors.Count == 0 && ModelState.Values.ElementAt(1).Errors.Count == 0 && ModelState.Values.ElementAt(2).Errors.Count == 0) {
                linkObj.UserID = User.Identity.GetUserId();
                db.Links.Add(linkObj);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(linkObj);
        }

        // GET: Links/Edit/5
        public ActionResult Edit(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Link linkObj = db.Links.Find(id);
            if (linkObj == null) {
                return HttpNotFound();
            }
            return View(linkObj);
        }

        // POST: Links/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,nazwa,url,publiczny")] Link linkObj) {
            if (ModelState.Values.ElementAt(0).Errors.Count == 0 && ModelState.Values.ElementAt(1).Errors.Count == 0 && ModelState.Values.ElementAt(2).Errors.Count == 0) {
                linkObj.UserID = (from l in db.Links
                               where l.Id == linkObj.Id
                               select l.UserID).ToArray()[0];
                db.Entry(linkObj).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(linkObj);
        }

        // GET: Links/Delete/5
        public ActionResult Delete(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Link linkObj = db.Links.Find(id);
            if (linkObj == null) {
                return HttpNotFound();
            }
            return View(linkObj);
        }

        // POST: Links/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id) {
            Link linkObj = db.Links.Find(id);
            db.Links.Remove(linkObj);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
