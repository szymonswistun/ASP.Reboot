﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ASP.Reboot.Models;
using Microsoft.AspNet.Identity;
using System.Data.SqlClient;
using System.Configuration;

namespace ASP.Reboot.Controllers {
    public class NotesController : Controller {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Notes
        [Route("notes/private/list")]
        public ActionResult Index() {
            if (User.Identity.IsAuthenticated) {
                string id = User.Identity.GetUserId();
                var dbList = (from n in db.Notes
                              where n.userID == id
                              select n).AsEnumerable<Note>();

                return View(dbList);
            }
            else {
                return RedirectToRoute("Default");
            }
        }
        [Route("notes/public/list")]
        public ActionResult PublicList() {
            string uid = User.Identity.GetUserId();
            var list = ((User.IsInRole("Admin"))?
                (from l in db.Notes
                 select l) :
                (from l in db.Notes
                        where l.publiczny == true || l.userID == uid
                        select l)).AsEnumerable<Note>();
            ViewData["Admin"] = User.IsInRole("Admin");

            return View(list);
        }

        // GET: Notes/Details/5
        public ActionResult Details(int? id) {
            if (id == null) {
                return RedirectToRoute("Default");
            }
            Note note = db.Notes.Find(id);
            if (note == null) {
                return HttpNotFound();
            }
            var uId = note.userID;
            var name = (from u in db.Users
                        where u.Id == uId
                        select u);
            ViewData["user"] = name.AsEnumerable().ElementAt(0).UserName;
            return View(note);
        }

        // GET: Notes/Create
        public ActionResult Create() {
            return View();
        }

        // POST: Notes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,nazwa,tresc,publiczny")] Note note) {
            if (ModelState.Values.ElementAt(0).Errors.Count == 0 && ModelState.Values.ElementAt(1).Errors.Count == 0 && ModelState.Values.ElementAt(2).Errors.Count == 0) {
                note.userID = User.Identity.GetUserId();
                db.Notes.Add(note);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(note);
        }

        // GET: Notes/Edit/5
        public ActionResult Edit(int? id) {
            if (id == null) {
                return RedirectToRoute("Default");
            }
            Note note = db.Notes.Find(id);
            if (note == null) {
                return HttpNotFound();
            }
            return View(note);
        }

        // POST: Notes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,nazwa,tresc,publiczny")] Note note) {
            if (ModelState.Values.ElementAt(0).Errors.Count == 0 && ModelState.Values.ElementAt(1).Errors.Count == 0 && ModelState.Values.ElementAt(2).Errors.Count == 0) {
                note.userID = (from n in db.Notes
                               where n.Id == note.Id
                               select n.userID).ToArray()[0];
                db.Entry(note).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(note);
        }

        // GET: Notes/Delete/5
        public ActionResult Delete(int? id) {
            if (id == null) {
                return RedirectToRoute("Default");
            }
            Note note = db.Notes.Find(id);
            if (note == null) {
                return HttpNotFound();
            }
            return View(note);
        }

        // POST: Notes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id) {
            Note note = db.Notes.Find(id);
            db.Notes.Remove(note);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
