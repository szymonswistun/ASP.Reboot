﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace System.Web.Mvc {

    public partial class Helper {
        public static MvcHtmlString LinkHelper(string url) {
            return new MvcHtmlString(String.Format(@"<a url=""{0}"">{0}</a>", url));
        }
    }
}