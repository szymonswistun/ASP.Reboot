﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASP.Reboot {
    public class FirstCapital : ValidationAttribute {

        public FirstCapital() { }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext) {
           if (!(value is string)) {
                return new ValidationResult(this.FormatErrorMessage("Invalid field type"));
            }
            string val = (string)value;
            if(val[0].ToString() != val[0].ToString().ToUpper()) {
                return new ValidationResult(this.FormatErrorMessage("Musi zaczynać się z wielkiej litery"));
            }
            return null;
        }

    }
}